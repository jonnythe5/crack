#include <stdio.h>
#include <stdlib.h>
#include "md5.h"
#include <string.h>

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Must supply a src and dest filename\n");
        exit(1);
    }
    //Open first file for reading
    FILE *src = fopen(argv[1], "r");
    if (!src)
    {
        printf("can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    //Open second file for writing
    FILE * dest = fopen(argv[2], "w");
    if (!dest)
    {
        printf("Can't open %s for writing.\n", argv[2]);
        exit(1);
    }
    //
    
    char word[100];
    //char *hash = md5(word, strlen(word));
    char *hash;
    int amountofHashes = 0;
    //fscanf("%s", word);
    
    while (fgets(word, 100, src) != NULL)
    {
        
        char newWord[100];
        int i = 0;
        while (word[i] != '\n')
        {
            newWord[i] = word[i];
            //printf("Inner Loop, %s", newWord); //Debugs whatever is passed into the loop
            i++;
        }
        amountofHashes++;
        newWord[i] = '\0';
        hash = md5(newWord, strlen(newWord));
//        printf("%lu", strlen(newWord));
        
//        printf("Password: %s", newWord);
//        printf("    Hash: %s\n", hash);
        fprintf(dest, "%s\n", hash);
    }
    
    fclose(src);
    fclose(dest);
    
    printf("%d hashes generated.\n", amountofHashes);

}

//clang hashpass.c md5.c -o hashpass -l crypto
//./hashpass rockyou100.txt hashes.txt